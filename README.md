# Todo List #

Build a todo list using [vuejs](https://vuejs.org/)

### Setup Options ###

* Laravel App (using [Laravel installer](https://laravel.com/docs/8.x#getting-started-on-macos))
* VueJS App (using [Vue CLI](https://cli.vuejs.org/))

### Requirements ###

* Build a todo list
* Use VueJS components

### Todo List ###

- Add item to list using form input
- Display list of items
- Mark item as complete (with visual indication)
    - Move to bottom (and/or)
    - Strikethrough (and/or)
    - Change color
- Remove items from list

### Resources ###

* [https://scotch.io/tutorials/build-a-to-do-app-with-vue-js-2](https://scotch.io/tutorials/build-a-to-do-app-with-vue-js-2)
* [https://laracasts.com/series/learning-vuejs/episodes/1](https://laracasts.com/series/learning-vuejs/episodes/1)